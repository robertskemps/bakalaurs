<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class AdminController extends Controller
{
    public function index()
    {
        return view('admin.home');
    }

    public function limits()
    {
        $current = DB::table('sensor_range')->orderBy('id', 'desc')->first();
        $list = DB::table('sensor_range')->orderBy('id', 'desc')->take(8)->get();
        return view('admin.limits', ['from' => $current->min_value, 'to' => $current->max_value, 'list' => $list]);
    }

    public function reports(Request $request)
    {
        $dbReturn = $this->returnDatabaseResult($request->time);
        $readings = $dbReturn[0];
        $times = $dbReturn[1];
        $retVariables = $dbReturn[2];

        $readingArr = [];
        foreach ($readings as $reading) {
            array_push($readingArr, $reading->reading);
        }
        $timeArray = [];
        foreach ($times as $time) {
            array_push($timeArray, $time->created_at);
        }

        if ($retVariables) {
            $ret[0] = $readingArr;
            $ret[1] = $timeArray;
            return $ret;

        } else {
            return view('admin.reports', ['readings' => $readingArr, 'times' => $timeArray]);
        }

    }

    public function returnDatabaseResult($time)
    {
        $currentDate = Carbon::now();
        $retVariables = true;
        //Switch to get the time selected
        switch ($time) {
            case 'day':
                $agoDate = $currentDate->subDays(1)->setTime(00, 00, 00);
                break;
            case 'week':
                $agoDate = $currentDate->subDays(7)->setTime(00, 00, 00);
                break;
            case 'month':
                $agoDate = $currentDate->subMonth()->setTime(00, 00, 00);
                break;
            default:
                $agoDate = $currentDate->subHours(12);
                $retVariables = false;
                break;
        }

        //Select the values
        $currentDate = Carbon::now();
        if ($agoDate != null) {
            $readings = DB::table('sensor_reading')->select('reading')->whereBetween('created_at', [$agoDate, $currentDate])->get();
            $times = DB::table('sensor_reading')->select('created_at')->whereBetween('created_at', [$agoDate, $currentDate])->get();
        }
        $ret[0] = $readings;
        $ret[1] = $times;
        $ret[2] = $retVariables;
        return $ret;
    }

    public function reportsTable(Request $request)
    {
        if (!$request->page) {
            $page = 0;
        } else {
            $page = $request->page;
        }
        $dbReturn = $this->returnDatabaseResultTable($request->time, $page);

        return view('admin.table', ['readings' => $dbReturn]);
    }

    public function returnDatabaseResultTable($time, $page)
    {
        $currentDate = Carbon::now();
        $retVariables = true;
        //Switch to get the time selected
        switch ($time) {
            case 'day':
                $agoDate = $currentDate->subDays(1)->setTime(00, 00, 00);
                break;
            case 'week':
                $agoDate = $currentDate->subDays(7)->setTime(00, 00, 00);
                break;
            case 'month':
                $agoDate = $currentDate->subMonth()->setTime(00, 00, 00);
                break;
            default:
                $agoDate = $currentDate->subHours(12);
                $retVariables = false;
                break;
        }

        //Select the values
        $currentDate = Carbon::now();
        if ($agoDate != null) {
            $readings = DB::table('sensor_reading')->select('*')
                ->whereBetween('created_at', [$agoDate, $currentDate])->paginate(10, ['*'], 'page', $page);
        }
        return $readings;
    }

    public function addRange(Request $request)
    {
        $ret = true;

        if($request->from == '' || $request->from == null){
            $ret = 'Pirmais skaitlis ir tukšs!';
            if(!is_numeric((int)$request->from)){
                $ret = 'Pirmais skaitlis nekorekti ievadīts!';
            }
        }
        if($request->to == '' || $request->to == null){
            $ret = 'Otrais skaitlis ir tukšs!';
            if(!is_numeric((int)$request->to)){
                $ret = 'Otrais skaitlis nekorekti ievadīts!';
            }
        }

        if($ret == true){
            DB::table('sensor_range')->insert(['min_value' => $request->from, 'max_value' => $request->to, 'created_at' => Carbon::now()]);
            return \Response::json(array("errors" => ''), 200);
        }else{
            return \Response::json(array("errors" => $ret), 422);
        }
    }
}
