<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class ApiController extends Controller
{
    /**
     * Function that takes the request from arduino and convert the sensor values into percentage and inserts the value
     * in the db. Also returns the user set min and max moisture levels for the soil.
     * @param Request $request
     * @return string
     */
    public function apiCall(Request $request) {
        //Calculate the moisture percentage of the sensor and insert the value into db
        $percentage = round((100*$request->sensor)/700, 0);
        DB::table('sensor_reading')->insert(['reading' => $percentage, 'created_at' => Carbon::now()->toDateTimeString()]);

        //Read from db the last set min/max values
        $values =  DB::table('sensor_range')->orderBy('id', 'desc')->first();
        return '^'.$values->min_value.'^'.$values->max_value.'^';
    }
}
