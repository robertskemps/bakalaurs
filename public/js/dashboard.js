/* global variables*/
var firstload = true;
var fromStyle;
var toStyle;
var time;

/* On click function that checks*/
$('.chart-period').on ('click', function () {
    time = $(this).val();
});

$(function () {
    time = 'day';
    $('[data-toggle="tooltip"]').tooltip();
    fromStyle = $('#from').attr('style');
    toStyle = $('#from').attr('style');
    if (window.location.href.indexOf("report") > -1) {
        onPageLoad();
    }
});

function onPageLoad() {
    // Graphs
    var ctx = document.getElementById('myChart');

    // eslint-disable-next-line no-unused-vars
    var myChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: times,
            datasets: [{
                data: readings,
                lineTension: 0,
                backgroundColor: 'transparent',
                borderColor: '#007bff',
                borderWidth: 3,
                pointBackgroundColor: '#007bff'
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: false
                    }
                }]
            },
            legend: {
                display: false
            }
        }
    })
}

$(".chart-period").on("click", function (e) {
    $('.chart-period').removeClass('active');
    $(this).addClass('active');
    var fired_button = $(this).val();
    var checkbox = $('#myonoffswitch').is(':checked');
    if (checkbox) {
        graphAjax(fired_button);
    } else {
        tableAjax(fired_button);
    }
});

function tableAjax(fired_button) {
    $.ajax({
        type: 'post',
        url: '/reports/table',
        data: {
            'time': time,
            '_token': $('meta[name="csrf-token"]').attr('content'),
        },
        success: function (response) {
            $('#myChart').css('display', 'none');
            $('#table').html(response);
            $('#table').css('display', 'flex');
        },
    });
}

$('.page-link').on('click', function () {
    console.log('suns');
    $.ajax({
        url: '/readings/tablePaginate/?page=' + pageNumber
    }).done(function (data) {
        $('#readingsTable').html(data);
    });
});

function graphAjax(fired_button) {
    $.ajax({
        type: 'post',
        url: '/reports',
        data: {
            'time': time,
            '_token': $('meta[name="csrf-token"]').attr('content'),
        },
        success: function (response) {
            $('#table').css('display', 'none');
            $('#myChart').css('display', 'flex');
            readings = response[0];
            times = response[1];
            onPageLoad();
        },
    });
}

$("#myonoffswitch").on("change", function () {
    //Fales = table; True = graph;
    var value = $(this).is(':checked');
    var buttonVal = $('.active').val();
    if (value) {
        if (time != undefined) {
            graphAjax(buttonVal);
        } else {
            graphAjax('day');
        }
    } else {
        if (time != undefined) {
            tableAjax(buttonVal);
        } else {
            tableAjax('day');
        }
    }
});

//Create ajax call so we can swap pages without reaload
$(document).on('click', '.paginateReportsTable a', function (e) {
    e.preventDefault();
    var page = $(this).attr('href').split('page=')[1];
    console.log(time);
    getPage(page, time);
});

function getPage(pageNumber, time) {
    $.ajax({
        url: '/reports/tablePaginate/?page=' + pageNumber + '&time=' + time
    }).done(function (data) {
        $('#table').html(data);

    });
}

$('#submitForm').on('click', function () {
    //Set variables
    var from = $('#from').val();
    var to = $('#to').val();
    var focus = false;
    var ajaxTrue = true;

    //Checking if inputs are defined and
    if (from == '' || from == undefined || from == null || from <= 0) {
        $('#from').css({
            'border-color': 'red',
            'box-shadow': '0 1px 1px red inset, 0 0 8px red',
            'outline': '0 none'
        });
        ajaxTrue = false;
    } else {
        document.getElementById("from").setAttribute("style", toStyle);
        ajaxTrue = true;
    }

    if (to == '' || to == undefined || to == null || from >= to || to > 100) {
        $('#to').css({
            'border-color': 'red',
            'box-shadow': '0 1px 1px red inset, 0 0 8px red',
            'outline': '0 none'
        });
        ajaxTrue = false;
    } else {
        document.getElementById("to").setAttribute("style", toStyle);
        ajaxTrue = true;
    }
    //Post values to server
    if (ajaxTrue == true) {
        $.ajax({
            type: 'post',
            url: '/add/range',
            data: {
                'from': from,
                'to': to,
                '_token': $('meta[name="csrf-token"]').attr('content'),
            },
            success: function (response) {
                console.log(response);
                $('#successAlert').fadeIn().delay(5000).fadeOut();
            },
            error: function (response) {
                $('#failedAlert').fadeIn().delay(5000).fadeOut();
            },
        });
    }
});
