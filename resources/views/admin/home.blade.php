@extends('layouts.admin')
@section('content')
    <main role="main" class="col-md-9 ml-sm-auto col-lg-10 ">
        <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <h1 class="h2">Sākums</h1>
        </div>
        <div class="col-12">
            <h6 class="h6">
                Esiet sveicināti Automatizētās telpaugu laistīšanas sistēmas prototipa administrācijas panelī.
            </h6>
            <p class="justify-content pt-2">
                Šis administrācijas panelis tika izveidots kā kontrolēšanas un datu analizēšanas rīks priekš LLU
                studenta Roberta Kempa bakalaura darba - "Automatizētās telpaugu laistīšanas sistēmas prototipa
                izstrāde" vajadzībām.
            </p>
            <p class="justify-content pt-5">
                Administrācijas panēlī ir iespājms veikt attiecīgās darbības:
            </p>
            <ul>
                <li>Sadaļā diapazons:</li>
                <ul>
                    <li>Ievadīt mitruma līmeņa diapazonu (%) kuru prototips sāks ievērot ar tā nākošo pieprasījumu
                        serverim;
                    </li>
                    <li>Apskatīt vēsturiskos mitruma līmeņa diapazona ierakstus.</li>
                </ul>
                <li>Atskaites :</li>
                <ul>
                    <li>Apskatīt vēsturiskos mitruma līmeņa lasījumus, dažādos laika periodos, tabulas un grafika veidā
                        (%).
                    </li>
                </ul>
            </ul>
        </div>
    </main>