@extends('layouts.admin')
@section('content')

    <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
        <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <h1 class="h2">Diapazons</h1>
        </div>
        <!-- Default switch -->
        <div class="col-12">
            <div id="successAlert" style="display: none;" class="alert alert-success">
                <strong>Diapazons saglabāts!</strong> Mitruma līmeņa diapazons tika veiksmīgi saglabāts, sākot ar
                nākošo pieprasījumu tas tiks izmantots jūsu arduino programmā.
            </div>
            <div id="failedAlert" style="display: none;" class="alert alert-danger">
                Radās kļūda saglabājot ievadīto diapazonu.
            </div>
            <div class="col-12">
                <label for="input-group">Ievadiet mitruma diapazonu</label>
            </div>
            <div class="input-group input-group-sm col-8 col-12 col-sm-10 col-md-8 col-lg-8">
                <input id="from" type="number" name="from" placeholder="{{$from}}" class="form-control col-3"
                       aria-label="Mitrums no"
                       data-toggle="tooltip" data-placement="bottom"
                       title="Skaitlim jābūt > 0 un < kā otrajam skailim">
                <div class="input-group-append from">
                    <span class="input-group-text">&percnt;</span>
                </div>
                <span style="vertical-align: center;" class="mr-1 ml-1">&nbsp; līdz &nbsp;</span>
                <input id="to" type="number" name="to" placeholder="{{$to}}" class="form-control col-3"
                       data-toggle="tooltip" data-placement="bottom"
                       title="Skaitlim jābūt > kā pirmajam skaitlim un <= 100">
                <div class="input-group-append to">
                    <span class="input-group-text">&percnt;</span>
                </div>
                <button id="submitForm" class="btn btn-primary btn-sm ml-3">Apstiprināt</button>
            </div>
        </div>

        <h4 class="h4 mt-5">Iepriekšējās vērtībās <small>(pēdējās 8 izmaiņas)</small></h4>
        <table id="readingsTable" class="table">
            <thead>
            <tr class="text-center">
                <th class="w-10">ID</th>
                <th class="w-30">Mitruma līmenis no (%)</th>
                <th class="w-30">Mitruma līmenis līdz (%)</th>
                <th class="w-30">Datums un laiks</th>
            </tr>
            </thead>
            <tbody>
            @foreach($list as $val)
                <tr class="text-center">
                    <td>{{$val->id}}</td>
                    <td>{{$val->min_value}}</td>
                    <td>{{$val->max_value}}</td>
                    <td>{{$val->created_at}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </main>
    <script>

    </script>