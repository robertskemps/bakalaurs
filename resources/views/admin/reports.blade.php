@extends('layouts.admin')
@section('content')

    <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
        <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <h1 class="h2">Atskaites</h1>
        </div>
        <!-- Default switch -->
        <div class="onoffswitch">
            <input type="hidden" name="onoffswitch" value="false">
            <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch" checked>
            <label class="onoffswitch-label" for="myonoffswitch">
                <span class="onoffswitch-inner"></span>
                <span class="onoffswitch-switch"></span>
            </label>
        </div>
        <div class="btn-group" role="group" data-toggle="button" aria-label="Basic example">
            <button type="button" value="day" class="btn btn-primary chart-period">Diena</button>
            <button type="button" value="week" class="btn btn-primary chart-period">Nedēļa</button>
            <button type="button" value ="month"class="btn btn-primary chart-period">Mēnesis</button>
        </div>
        <canvas class="my-4" id="myChart" style="position: relative; height:50vh; width:80vw"></canvas>
        <div id="table">

        </div>
    </main>
    <script>
        var readings = <?php echo json_encode($readings); ?>;
        var times = <?php echo json_encode($times); ?>;
    </script>