<table class="table">
    <thead>
    <tr class="text-center">
        <th class="w-20">Nr. p.k.</th>
        <th class="w-30">Mitruma līmenis (%)</th>
        <th class="w-50">Datums un laiks</th>
    </tr>
    </thead>
    <tbody>
    <?php $i = 1; ?>
    @foreach($readings as $key => $reading)
        <tr class="text-center">
            <td>{{$reading->id}}</td>
            <td>{{$reading->reading}}</td>
            <td>{{$reading->created_at}}</td>
        </tr>
        <?php $i++; ?>
    @endforeach
    </tbody>
</table>
<!--Paginate starts -->
<nav aria-label="paginate" class="text-center">
    <ul class="paginateReportsTable pagination justify-content-center">
        {{$readings->links()}}
    </ul>
</nav>
<!--Paginate ends -->