@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Ievadiet jūsu reģistrēto E - pasta adresi</div>

                <div class="card-body">
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            Konta atjaunošanas saite ir nosūtīta uz jūsu noradīto E - pastu
                        </div>
                    @endif

                    Pirms turpiniet apskataties E - pastā apstriprinājuma ziņu.
                    Ja nesaņēmāt E - pastu, <a href="{{ route('verification.resend') }}">Nospiediet, lai nosūtītu vēlreiz</a>.
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
