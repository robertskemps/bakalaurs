<!doctype html>
<html lang="en">
<head>
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <title>Administrācijas panelis</title>
    <!-- Bootstrap core CSS -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <link rel="canonical" href="https://getbootstrap.com/docs/4.3/examples/dashboard/">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <!-- Jquery -->
    <script
            src="https://code.jquery.com/jquery-3.4.0.min.js"
            integrity="sha256-BJeo0qm959uMBGb65z40ejJYGSgR7REI4+CW1fNKwOg="
            crossorigin="anonymous"></script>
    <!-- Bootstrap core JS -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js" integrity="sha384-xrRywqdh3PHs8keKZN+8zzc5TX0GRTLCcmivcbNJWm2rs5C8PRhcEn3czEjhAO9o" crossorigin="anonymous"></script>

    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }
    </style>
    <!-- Custom styles for this template -->
    <link href="css/dashboard.css" rel="stylesheet">

    <link rel="apple-touch-icon" sizes="180x180" href="favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="favicon/favicon-16x16.png">
    <link rel="manifest" href="favicon/site.webmanifest">
    <link rel="mask-icon" href="favicon/safari-pinned-tab.svg" color="#5bbad5">
</head>
<body class="{{ Request::path() == 'login' ? 'background-image' : '' }}">

<nav class="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow d-none d-md-flex">
    <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="/admin">Administrācija</a>
    <ul class="navbar-nav px-3">
        <li class="nav-item text-nowrap">
            <a class="nav-link" href="{{ route('logout') }}"
               onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                Izlogoties
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        </li>
    </ul>
</nav>
<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark d-flex d-md-none">
    <a class="navbar" href="/admin" style="color: white;">Administrācija</a>
    <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="navbar-collapse collapse" id="navbarCollapse">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link @if(Request::url() == 'http://roberts.tk/admin') active @endif" href="/admin">
                    <i class="fas fa-home"></i>
                    Sākums @if(Request::url() == 'admin') <span class="sr-only">(current)</span> @endif
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link @if(Request::url() == 'http://roberts.tk/limits') active @endif" href="/limits">
                    <i class="fas fa-arrows-alt-h"></i>
                    Diapazons @if(Request::url() == 'limits') <span class="sr-only">(current)</span> @endif
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link @if(Request::url() == 'http://roberts.tk/reports') active @endif" href="/reports">
                    <i class="fas fa-chart-area"></i>
                    Atskaites @if(Request::url() == 'reports') <span class="sr-only">(current)</span> @endif
                </a>
            </li>
            <a class="nav-link" href="{{ route('logout') }}"
               onclick="event.preventDefault(); document.getElementById('logout-form').submit();" style="text-align: right;">
                <i class="fas fa-sign-out-alt"></i>
                Izlogoties
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        </ul>

    </div>
</nav>
<div class="container-fluid">
    <div class="row">
        <nav class="col-md-2 d-none d-md-block bg-light sidebar">
            <div class="sidebar-sticky">
                <ul class="nav flex-column">
                    <li class="nav-item">
                        <a class="nav-link @if(Request::url() == 'http://roberts.tk/admin') active @endif" href="/admin">
                            <i class="fas fa-home"></i>
                            Sākums @if(Request::url() == 'admin') <span class="sr-only">(current)</span> @endif
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link @if(Request::url() == 'http://roberts.tk/limits') active @endif" href="/limits">
                            <i class="fas fa-arrows-alt-h"></i>
                            Diapazons @if(Request::url() == 'limits') <span class="sr-only">(current)</span> @endif
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link @if(Request::url() == 'http://roberts.tk/reports') active @endif" href="/reports">
                            <i class="fas fa-chart-area"></i>
                            Atskaites @if(Request::url() == 'reports') <span class="sr-only">(current)</span> @endif
                        </a>
                    </li>

                </ul>
            </div>
        </nav>
        @yield('content')
    </div>
</div>
<script>window.jQuery || document.write('<script src="/docs/4.3/assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/feather-icons/4.9.0/feather.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.min.js"></script>
<script src="js/dashboard.js"></script>
</body>
</html>