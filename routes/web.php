<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


//Api routes
Route::get('/api', 'ApiController@apiCall');

Auth::routes();

//Get routes
Route::get('/admin', 'AdminController@index')->middleware('auth');
Route::get('/reports', 'AdminController@reports')->middleware('auth');
Route::get('/limits', 'AdminController@limits')->middleware('auth');

//For table ajax request
Route::get('/reports/tablePaginate', 'AdminController@reportsTable')->middleware('auth');
Route::get('/reports/tablePaginate', 'AdminController@reportsTable')->middleware('auth');
Route::get('/home', 'AdminController@index')->middleware('auth');

//Post Routes
Route::post('/reports', 'AdminController@reports')->middleware('auth');
Route::post('/reports/table', 'AdminController@reportsTable')->middleware('auth');
Route::post('/add/range', 'AdminController@addRange')->middleware('auth');
